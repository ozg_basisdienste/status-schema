# Status schema

> :warning: **This project is in alpha state**: The contents of this repository have not yet reached a state suitable for use in production.

This schema defines status values for applications under the [Onlinezugangsgesetz (OZG)](https://www.gesetze-im-internet.de/ozg/) law.

The schema is used to implement the status monitor (Statusmonitor) feature as defined in [IT-Planungsrat](https://www.it-planungsrat.de/) decision [2018/40](https://www.it-planungsrat.de/beschluss/beschluss-2018-40).
The implementation of the status monitor feature was mandated in decision [2023/29](https://www.it-planungsrat.de/beschluss/beschluss-2023-29).

# How to work with this schema

To view this schema and visualize it in a structured form, you can use the [react-jsonschema-form playground](https://rjsf-team.github.io/react-jsonschema-form/) (just copy the contents of [status.schema.json](./status.schema.json) to the `JSONSchema` text area).

# License
This work is licensed under the [EUPL](LICENSES/EUPL-1.2.md).